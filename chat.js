//Settings

// Config starts here
var endpoint = 'xmpphp/chat_backend.php'
var history_size = 20;
// Config ends here

var InputBuffer = new Array();
var History = new Array();
var HistoryPosition = 0;

function addBuffer(message) {
	InputBuffer.push(message);
}

function outBuffer() {
	InputBuffer.reverse();
	while (InputBuffer.length > 0) {
		raw_message = InputBuffer.pop();
		$('#inputfield input').addClass('loading');
		$.getJSON(endpoint, {op: 'SendMessage', message : raw_message}, function(data){
			    addChatMessage($("input[name='nick']").attr('value'), raw_message);
			    $('#inputfield input').removeClass('loading');
			  });
	}
	setTimeout('outBuffer()',500);
}

function now() {
	var timestamp = new Date();
	timestamp = timestamp.format('HH:MM:s');
	return timestamp;
}

function addError(error) {
	var message = '<div class="logentry error"><span class="timestamp">'+now()+'</span><span class="logmessage">'+error+'</span></div>';
	$(message).appendTo('#chatlog');
	cleanLog();
}

function addStatus(status) {
	var message = '<div class="logentry status"><span class="timestamp">'+now()+'</span><span class="logmessage">'+status+'</span></div>';
	$(message).appendTo('#chatlog');
	cleanLog();
}

function addSuccess(success) {
	var message = '<div class="logentry success"><span class="timestamp">'+now()+'</span><span class="logmessage">'+success+'</span></div>';
	$(message).appendTo('#chatlog');
	cleanLog();
}

function addLog(logEntry){
  var fullentry = '<div class="logEntry"><span class="timestamp">'+now()+'</span><span class="logmessage">'+logEntry+'</span></div>';
  $(fullentry).appendTo('#chatlog');
  cleanLog();
};

function cleanLog() {
	if ($('.logEntry').length > history_size) {
	    $('.logEntry:last').remove();
	  }
}

function addChatMessage(nick, message) {
  var logentry = '<span class="nick">'+nick.split('@')[0]+'</span><span class="message">'+message+'</span>';
  addLog(logentry);
}

function handleEvent() {
  $.getJSON(endpoint,{'op' : 'GetEvents'}, function(data) {
	      if(data['status'] == 0) {
		addChatMessage(data['nick'], data['message']);
	      }
	      window.setTimeout('handleEvent()',1000);
	    });
};

$(document).ready(function(){
		    $('#nojs').css('display','none');
		    $('#chatbox').css('display','block');
		    $('#messagebox').attr('value','Enter your message here.');
		    $('#messagebox').bind('focus',function() {
					    if ($('#messagebox').attr('value') == 'Enter your message here.') {
					      $('#messagebox').attr('value','');
					    } else {
					      $('#messagebox').select();
					    }
					  });

		    $("input[name='textinput']").bind('keypress', function(key){
				if (key.keyCode == 38) {
					if ((History.length > 0) && (HistoryPosition > 0)){
						HistoryPosition--;
						$('#textinput').attr('value',History[HistoryPosition]);
					}
				} else if(key.keyCode == 40) {
					if ((History.length > 0) && (HistoryPosition < History.length)){
						HistoryPosition++;
						$('#textinput').attr('value',History[HistoryPosition]);
					}
				} else if(key.keyCode == 13) {
					var message = $('#textinput').attr('value').replace(/</g, '&lt;').replace(/>/g, '&gt;');;
					addBuffer(message);
					History.push(message);
					$('#textinput').addClass('loading');
					$('#textinput').attr('value','');
					$('#textinput').focus();
					HistoryPosition = History.length;
				} else {
					HistoryPosition = History.length;
				}
		    });

		    $("input[name='submit_nick']").bind('click',function(e){
							  $('#asknick').fadeOut('fast');
							  $('#chatlog-fieldset legend').text('Welcome ' + $("input[name='nick']").attr('value'));
							  $('#chatlog-fieldset').fadeIn('fast');
							  addStatus('Connecting...');
							  // establish a connection to the ajax socket
							  $('#messagebox input').addClass('loading');
							  $.getJSON(endpoint,{'op' : 'InitConnection', 'nick' : $("input[name='nick']").attr('value')}, function(data) {
								      if(data['status'] == 0) {
									addStatus(data['message']);
									handleEvent();
									outBuffer();
									$('#messagebox input').removeClass('loading');
								      }
								    });
							});
		  });
