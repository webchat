<?php
  // This script connects to a jabberserver via bosh and send messages
  // to a participant and recieves messages and relays to it the
  // webchat user.

  // This takes the following arguments:
  // * op
  //   Tell the bridge what to do. Can have the following parameters:
  //   * InitConnection
  //     Initialise the connection
  //   * GetEvents
  //     Wait till we recieve an event
  //   * SendMessage
  //     Send a message to the other side
  //   * Disconnect
  //     Disconnects the session
  // * message
  //   If op is SendMessage contains the message to be send

  // This Script returns JSON containing the following parameters:
  // * status:
  //   If greater than 0: some error happend
  //   If equals to 0: everything fine
  // * message
  //   Containts chat messages or presence informations
  // * error_msg
  //   A errorstring to display the user


  /* Settings */
$_SETTINGS['to'] = 'tim@boese-ban.de';


ini_set('display_errors','true');

error_reporting(E_ALL & E_STRICT);

function checkStatus() {

}

function handleConnection($conn) {
  try {
    $conn->connect('http://localhost:5280/http-bind', 1, true);
    $payloads = $conn->processUntil(array('message', 'presence', 'end_stream', 'session_start'),3);

    $conn->presence($status="Cheese!");
    if ((is_array($payloads)) && (count($payloads) > 0)) {
      foreach($payloads as $event) {
	$pl = $event[1];
	switch($event[0]) {
	case 'message':
	  $retval = array('status' => 0, 'message' => $pl['body'], 'nick' => $pl['from']);
	  break;
	case 'presence':
	  $retval = array('status' => 0, 'message' => sprintf('%s: %s', $pl['show'], $pl['status']));
	  break;
	case 'session_start':
	  $conn->getRoster();
	  $conn->sendVCard(array('nickname' => $_SESSION['chat_nick'], 'fn' => $_SESSION['chat_nick']));
	  $conn->presence($status="Cheese!");
	  $retval = array('status' => 0, 'message' => 'Connection established');
	  break;
	default:
	  // got nothing, we need to return something, so the connection stays alive.
	  $retval = array('status' => -1, 'message' => '');
	  break;
	}
	if (is_array($retval)) {
	  echo json_encode($retval);
	} else {
	  // got nothing, we need to return something, so the connection stays alive.
	  echo json_encode(array('status' => -1, 'message' => ''));
	}
      }
    } else {
      // got nothing, we need to return something, so the connection stays alive.
      echo json_encode(array('status' => -1, 'message' => ''));
    }
  } catch(XMPPHP_Exception $e) {
    echo json_encode(array('status' => 1, 'message' => sprintf('Connection error: %s', $e->getMessage())));
  }

  $conn->saveSession();
}



$now = microtime(true);
session_start();
$valid_ops = array('InitConnection', 'GetEvents', 'SendMessage', 'Disconnect');
$op = $_GET['op'];
if (in_array($op,$valid_ops)) {
  // activate full error reporting
  error_reporting(E_ALL & E_STRICT);

  include 'XMPPHP/BOSH.php';

  switch($op) {
  case 'InitConnection':
    $_SESSION = array();
    $_SESSION['chat_nick'] = $_GET['nick'];
    $conn = new XMPPHP_BOSH('localhost', 5222, $_SESSION['chat_nick'], '', 'xmpphp', 'localhost');
    $conn->autoSubscribe();

    handleConnection($conn);
    break;
  case 'GetEvents':
    $conn = new XMPPHP_BOSH('localhost', 5222, $_SESSION['chat_nick'], '', 'xmpphp', 'localhost');
    $conn->autoSubscribe();
    handleConnection($conn);
    break;
  case 'SendMessage':
    $message = sprintf('<%s> %s', $_SESSION['chat_nick'], $_GET['message']);
    $conn = new XMPPHP_BOSH('localhost', 5222, $_SESSION['chat_nick'], '', 'xmpphp', 'localhost');
    $conn->autoSubscribe();
    try {
      //printf("9: %f\n", microtime(true) - $now);
      $conn->connect('http://localhost:5280/http-bind', 1, true);
      //printf("10: %f\n", microtime(true) - $now);
      $conn->message($_SETTINGS['to'], $message);
      //printf("11: %f\n", microtime(true) - $now);
      echo json_encode(array('status' => 0, 'message' => 'OK'));
    } catch(XMPPHP_Exception $e) {
      echo json_encode(array('status' => 1, 'message' => sprintf('Connection error: %s', $e->getMessage())));
      die();      
    }
    break;

  }

 }
